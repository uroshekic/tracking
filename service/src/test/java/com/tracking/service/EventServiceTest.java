package com.tracking.service;

import java.time.LocalDateTime;
import java.util.Optional;

import com.tracking.service.db.model.AccountEntity;
import com.tracking.service.db.repository.AccountRepository;
import com.tracking.service.dto.EventDto;
import com.tracking.service.kafka.KafkaService;
import com.tracking.service.service.EventService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class EventServiceTest {

    private AccountRepository accountRepository;

    private EventService eventService;

    @BeforeEach
    public void setUp() {
        accountRepository = Mockito.mock(AccountRepository.class);
        final KafkaService kafkaService = Mockito.mock(KafkaService.class);
        eventService = new EventService(accountRepository, kafkaService);
    }


    @Test
    public void testNullEventThrowsException() {
        Assertions.assertThrows(
                NullPointerException.class,
                () -> eventService.storeEvent(null));
    }

    @Test
    public void testEventWithNullAccountIdThrowsException() {
        Assertions.assertThrows(
                RuntimeException.class,
                () -> eventService.storeEvent(new EventDto(null, LocalDateTime.now().toString(), "Data")));
    }

    @Test
    public void testEventWithEmptyAccountIdThrowsException() {
        Assertions.assertThrows(
                RuntimeException.class,
                () -> eventService.storeEvent(new EventDto(" ", LocalDateTime.now().toString(), "Data")));
    }

    @Test
    public void testEventWithNullDataThrowsException() {
        Assertions.assertThrows(
                RuntimeException.class,
                () -> eventService.storeEvent(new EventDto("id", LocalDateTime.now().toString(), null)));
    }

    @Test
    public void testEventWithoutDataThrowsException() {
        Assertions.assertThrows(
                RuntimeException.class,
                () -> eventService.storeEvent(new EventDto("id", LocalDateTime.now().toString(), "")));
    }

    @Test
    public void testEventWithNonExistentAccountThrowsException() {
        Mockito.when(accountRepository.findById(ArgumentMatchers.anyString()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(
                RuntimeException.class,
                () -> eventService.storeEvent(new EventDto("id", LocalDateTime.now().toString(), "Yes")));
    }

    @Test
    public void testEvent() {
        final AccountEntity accountEntity = new AccountEntity();
        Mockito.when(accountRepository.findById(ArgumentMatchers.anyString()))
                .thenReturn(Optional.of(accountEntity));

        eventService.storeEvent(new EventDto("id", LocalDateTime.now().toString(), "Yes"));
    }

}
