package com.tracking.service.db.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

@Entity
public class AccountEntity {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Getter
    @Setter(AccessLevel.PROTECTED)
    private String id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private Boolean active;

    public boolean isActive() {
        return (active != null) && active;
    }
}
