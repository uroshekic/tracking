package com.tracking.service.db.repository;

import com.tracking.service.db.model.AccountEntity;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<AccountEntity, String> {

}
