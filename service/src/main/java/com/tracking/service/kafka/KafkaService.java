package com.tracking.service.kafka;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.tracking.service.dto.EventDto;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.KafkaProducerException;
import org.springframework.kafka.core.KafkaSendCallback;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

@Component
public class KafkaService {

    private final KafkaTemplate<Integer, EventDto> template;

    private final String kafkaTopic = "myTopic";

    public KafkaService(final KafkaTemplate<Integer, EventDto> kafkaTemplate) {
        this.template = kafkaTemplate;
    }

    public void sendEvent(final EventDto data) {
        /*
        ListenableFuture<SendResult<Integer, EventDto>> future = template.send(kafkaTopic, data);
        future.addCallback(new KafkaSendCallback<Integer, EventDto>() {

            @Override
            public void onSuccess(final Object o) {

            }

            @Override
            public void onFailure(final Throwable ex) {

            }

            @Override
            public void onFailure(final KafkaProducerException ex) {

            }
        });
        // */

        try {
            template.send(kafkaTopic, data).get(10, TimeUnit.SECONDS);
            handleSuccess(data);
        }
        catch (ExecutionException e) {
            handleFailure(data, e.getCause());
        }
        catch (TimeoutException | InterruptedException e) {
            handleFailure(data, e);
        }
    }

    private void handleSuccess(final EventDto data) {

    }

    private void handleFailure(final EventDto data, final Throwable ex) {
        throw new RuntimeException("Sending to kafka topic failed", ex);
    }

}
