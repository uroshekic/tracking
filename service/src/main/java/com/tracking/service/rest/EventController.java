package com.tracking.service.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.tracking.service.dto.EventDto;
import com.tracking.service.service.EventService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EventController {

    private final EventService eventService;

    public EventController(final EventService eventService) {
        this.eventService = eventService;
    }

    @RequestMapping(value = "/events", method = RequestMethod.POST)
    @ResponseBody
    public EventDto storeEvent(@RequestBody final EventRestDto eventRestDto) {
        final EventDto event = new EventDto(eventRestDto.getAccountId(), LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME), eventRestDto.getData());
        eventService.storeEvent(event);
        return event;
    }

    @RequestMapping(value = "/accounts/random", method = RequestMethod.POST)
    @ResponseBody
    public void createRandomAccounts() {
        eventService.createRandomAccounts();
    }

}
