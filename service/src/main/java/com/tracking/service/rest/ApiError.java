package com.tracking.service.rest;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@JsonSerialize
@AllArgsConstructor
public class ApiError {

    private final HttpStatus status;
    private final String message;

}
