package com.tracking.service.rest;

import lombok.Data;

@Data
public class EventRestDto {

    private String accountId;
    private String data;

}
