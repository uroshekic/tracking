package com.tracking.service.service;

import java.util.Optional;
import java.util.Random;

import com.tracking.service.db.model.AccountEntity;
import com.tracking.service.db.repository.AccountRepository;
import com.tracking.service.dto.EventDto;
import com.tracking.service.kafka.KafkaService;
import lombok.NonNull;
import net.bytebuddy.utility.RandomString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Component
@Transactional
public class EventService {

    private final AccountRepository accountRepository;
    private final KafkaService kafkaService;

    private static Logger LOG = LoggerFactory.getLogger(EventService.class);

    public EventService(
            final AccountRepository accountRepository,
            final KafkaService kafkaService) {
        this.accountRepository = accountRepository;
        this.kafkaService = kafkaService;
    }

    public void storeEvent(final @NonNull EventDto event) {
        if (StringUtils.isEmpty(event.getAccountId())) {
            throw new RuntimeException("accountId is empty");
        }

        if (StringUtils.isEmpty(event.getData())) {
            throw new RuntimeException("data is empty");
        }

        final Optional<AccountEntity> accountOpt = accountRepository.findById(event.getAccountId());

        if (accountOpt.isEmpty()) {
            throw new RuntimeException("accountId does not exist");
        }

        final AccountEntity account = accountOpt.get();
        if (account.isActive()) {
            propagateEvent(event);
        }
    }

    private void propagateEvent(final EventDto data) {
        kafkaService.sendEvent(data);
    }

    public void createRandomAccounts(int count) {
        for (int i = 0; i < count; i++) {
            final AccountEntity account = new AccountEntity();
            account.setName("Account " + RandomString.make(10));
            account.setActive(new Random().nextFloat() < 0.5f);
            AccountEntity saved = accountRepository.save(account);
            LOG.info(String.format("Created account id=%s active=%s", saved.getId(), saved.getActive()));
        }
    }

    public void createRandomAccounts() {
        createRandomAccounts(10);
    }
}
