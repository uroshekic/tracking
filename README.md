# Tracking

## Requirements
The following dependencies are required to build and run the project:
- Java 11 with Maven,
- Docker and docker-compose.

Run Postgres, Kafka and Zookeeper:
```docker-compose up -d```

## Tracking Service
To run Tracking service cd into `service/` and execute:
```mvn spring-boot:run```

Service listens on port 8080 by default and features a [Swagger UI](http://localhost:8080/swagger-ui.html).

REST API exposes a POST /events endpoint to handle events.

Example request:
```
curl --location --request POST 'http://localhost:8080/events' \
--header 'Content-Type: application/json' \
--data-raw '{
    "accountId": "7f75ae1f-098e-4b58-b5e2-f7dab398bf68",
    "data": "Hello world 2!"
}'
```

It also exposes an endpoint to generate random accounts:
```
curl --location --request POST 'http://localhost:8080/accounts/random' \
--data-raw ''
```

## CLI client
To run CLI client, cd into the `cli` folder and execute:
```
mvn spring-boot:run
```

To run CLI client with filtering by accountId, execute:
```
mvn spring-boot:run -Drun.arguments="--accountId 4c1819cd-22a6-43ec-bec4-94977b1e0288"
```

Or build it first with:
```
mvn clean package
```

And then cd into `target/` and run with:
```
java -jar cli-0.0.1-SNAPSHOT.jar
```
```
java -jar cli-0.0.1-SNAPSHOT.jar --accountId 4c1819cd-22a6-43ec-bec4-94977b1e0288
```

## Performance tests
To run performance tests, [Apache JMeter](https://jmeter.apache.org/) is required.  
In JMeter, click File, and then Open, select the `load-test.jmx` file, and press "Start" button. 
