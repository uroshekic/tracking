package com.tracking.cli.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tracking.cli.dto.EventDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaService {

    private final String kafkaTopic = "myTopic";

    private String accountIdFilter = null;

    private static Logger LOG = LoggerFactory.getLogger(KafkaService.class);

    @KafkaListener(
            id = "id",
            topics = kafkaTopic,
            clientIdPrefix = "tracker-cli"
            // containerFactory = "eventKafkaListenerContainerFactory"
    )
    public void listen(final String data) {
        LOG.debug(data.toString());

        final EventDto event = deserializeEvent(data);
        if (event == null) {
            return;
        }

        final String dataAccountId = event.getAccountId();
        if (isAccountFilterEnabled() || dataAccountId.equals(accountIdFilter)) {
            System.out.println(data);
        }
    }

    public void setAccountFilter(String filter) {
        accountIdFilter = filter;
    }

    private boolean isAccountFilterEnabled() {
        return accountIdFilter == null;
    }

    private EventDto deserializeEvent(final String data) {
        try {
            return new ObjectMapper().readValue(data, EventDto.class);
        }
        catch (JsonProcessingException e) {
            // e.printStackTrace();
            return null;
        }
    }

}
